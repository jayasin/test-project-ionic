import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UserformComponent } from '../userform/userform.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  contactInformations = [];

  constructor(public modelCtrl: ModalController) {
    this.contactInformations = JSON.parse(localStorage.getItem("contacts"));
  }


 async openUserForm() {
  const modal = await this.modelCtrl.create({
    component: UserformComponent,
    cssClass: 'my-custom-class'
  })

  modal.onDidDismiss()
      .then((data) => {

        if(data.data) {
          console.log("the data===>", data.data)

          let existingdata = JSON.parse(localStorage.getItem("contacts"));
  
          if(existingdata) {
            existingdata.push(data.data);
          } else {
            existingdata = [data.data]
          }     
          localStorage.clear();
          localStorage.setItem("contacts", JSON.stringify(existingdata))
  
          this.contactInformations = JSON.parse(localStorage.getItem("contacts"));
  
        }

        
    });


  return await modal.present();
  }

}
