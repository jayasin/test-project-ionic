import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.scss'],
})
export class UserformComponent implements OnInit {
  name: string;
  email: string;
  phone: string;

  constructor(public modelController: ModalController) {}


  ngOnInit() {}

  async closeModal() {
    this.modelController.dismiss();
  }

  onSubmit() {
    if (this.name && this.phone && this.email) {
      this.modelController.dismiss({
        name: this.name,
        email: this.email,
        phone: this.phone,
      });
    }
  }

  nameChangeHandler(type: string, text: string) {
    if (type === 'name') {
      this.name = text;
    } else if (type === 'email') {
      this.email = text;
    } else if (type === 'phone') {
      this.phone = text;
    }
  }
}
